﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Otus_DZ4
{
    class Download
    {

        public async Task DownloadFileAsync(Dictionary<string, string> img, string host, string path)
        {
            using (WebClient loadclient = new WebClient())
            {
                Directory.CreateDirectory(path);
                string pathUrl;
                foreach (KeyValuePair<string, string> file in img)
                {
                    try
                    {
                        Regex regrefurl = new Regex(@"(http)(s?)\:\/\/(\S+)(\.jpeg|jpg|png|svg|gif)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                        MatchCollection res = regrefurl.Matches(file.Key);
                        if (res.Count > 0)
                        {
                            pathUrl = file.Key;
                        }
                        else
                        {
                            pathUrl = "http:" + file.Key;
                            if (!Uri.IsWellFormedUriString(pathUrl, UriKind.Absolute))
                            {
                                pathUrl = host + file.Key;
                            }
                        }
                        await loadclient.DownloadFileTaskAsync(new Uri(pathUrl), Path.Combine(path, file.Value));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Файл {file.Key} Ошибка: {e}");
                    }
                }

            }
        }
    }
}
