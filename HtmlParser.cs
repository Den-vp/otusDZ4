﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Otus_DZ4
{
    class HtmlParser
    {
        public Dictionary<string, string> GetListImg(string html)
        {
            Dictionary<string, string> urlimg = new Dictionary<string, string>();
            Regex regref = new Regex(@"<img[\s\S]+?src=[\''""](\S+.(jpeg|jpg|png|svg|gif))", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            MatchCollection res = regref.Matches(html);
            if (res.Count > 0)
            {
                foreach (Match match in res)
                {
                    Regex regName = new Regex(@"[^\/]+.(jpeg|jpg|png|svg|gif)");
                    MatchCollection name = regName.Matches(match.ToString());
                    if (name.Count == 1 && !urlimg.ContainsKey(match.Groups[1].Value.ToString()))
                    {
                        try
                        {
                            urlimg.Add(match.Groups[1].Value.ToString(), match.Index + name[0].ToString());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }                        
                    }
                }
            }
            return urlimg;
        }

    }
}
